/* declarar variables */

const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click',function(){
    //obtener los datos de los input
    let peso = document.getElementById('peso').value;
    let altura = document.getElementById('altura').value;

    //hacer el calculo
    let imc = peso / (altura *altura);

    //mostrar el dato
    document.getElementById('imc').value = imc;
})